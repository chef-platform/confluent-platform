# frozen_string_literal: true

name 'confluent-platform'
maintainer 'Chef Platform'
maintainer_email 'incoming+chef-platform/confluent-platform@'\
  'incoming.gitlab.com'
license 'Apache-2.0'
description 'Install and configure Confluent platform (Kafka)'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://gitlab.com/chef-platform/confluent-platform'
issues_url 'https://gitlab.com/chef-platform/confluent-platform/issues'
version '3.6.0'

chef_version '>= 14.0'

supports 'centos', '>= 7.1'
supports 'debian', '>= 10.0' # may work on earlier versions

depends 'cluster-search'
