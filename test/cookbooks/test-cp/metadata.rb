# frozen_string_literal: true

name 'test-cp'
maintainer 'Chef Platform'
maintainer_email 'incoming+chef-platform/confluent-platform@'\
  'incoming.gitlab.com'
license 'Apache-2.0'
description 'Help test confluent platform cookbook'
long_description 'Help test confluent platform cookbook'
source_url 'https://gitlab.com/chef-platform/confluent-platform'
issues_url 'https://gitlab.com/chef-platform/confluent-platform/issues'
version '1.0.0'
