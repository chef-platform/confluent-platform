# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2019 Make.org, 2019-2021 SBernard
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

# rubocop:disable Metrics/BlockLength

describe 'Kafka Daemon' do
  it 'is running' do
    expect(service('kafka')).to be_running
  end

  it 'is launched at boot' do
    expect(service('kafka')).to be_enabled
  end

  waiting('kafka', 9092)

  it 'is listening on port 9092' do
    expect(port(9092)).to be_listening
  end
end

describe 'Kafka Configuration' do
  describe file('/etc/kafka/server.properties') do
    its(:content) { should eq <<-PROP.gsub(/^ {4}/, '') }
    # Produced by Chef -- changes will be overwritten

    auto.create.topics.enable=false
    broker.id=-1
    confluent.support.customer.id=anonymous
    confluent.support.metrics.enable=false
    default.replication.factor=2
    group.initial.rebalance.delay.ms=0
    leader.imbalance.check.interval.seconds=600
    log.cleaner.enable=false
    log.dirs=/var/lib/kafka
    log.retention.check.interval.ms=300000
    log.retention.hours=168
    log.segment.bytes=1073741824
    num.io.threads=2
    num.network.threads=2
    num.partitions=2
    num.recovery.threads.per.data.dir=1
    offsets.topic.num.partitions=10
    offsets.topic.replication.factor=2
    request.timeout.ms=300000
    socket.receive.buffer.bytes=102400
    socket.request.max.bytes=104857600
    socket.send.buffer.bytes=102400
    transaction.state.log.min.isr=1
    transaction.state.log.replication.factor=2
    zookeeper.connect=zookeeper-kafka.kitchen:2181/kafka-kitchen
    zookeeper.connection.timeout.ms=60000
    PROP
  end

  describe file('/etc/kafka/log4j.properties') do
    its(:content) { should contain 'log4j.rootLogger=INFO, stdout' }
    its(:content) { should contain '# Kitchen=true' }
  end
end

describe 'Kafka Topic Creation' do
  zk = '--zookeeper zookeeper-kafka.kitchen/kafka-kitchen'

  {
    'test' => 'Topic: test	PartitionCount: 2	'\
      'ReplicationFactor: 2	Configs: ',
    'test-avro' => 'Topic: test-avro	PartitionCount: 2	'\
      'ReplicationFactor: 2	Configs: compression.type=lz4',
    'test-binary' => 'Topic: test-binary	PartitionCount: 4	'\
      'ReplicationFactor: 2	Configs: ',
    'test-connect' => 'Topic: test-connect	PartitionCount: 2	'\
      'ReplicationFactor: 1	Configs: '
  }.each_pair do |topic, result|
    it "Topic #{topic} should be created with good configuration" do
      cmd = "kafka-topics #{zk} --describe --topic #{topic}"
      expect(command(cmd).stdout.lines.first.chomp).to eq(result)
    end
  end

  it 'Topic test-todelete should not exist' do
    cmd = "kafka-topics #{zk} --describe --topic test-todelete"
    res = 'Error while executing topic command : '\
      "Topic 'test-todelete' does not exist as expected"
    expect(command(cmd).stdout.chomp).to eq(res)
  end
end

describe 'Kafka Cluster' do
  brokers = [1, 2].map do |i|
    "--broker-list kafka-kitchen-0#{i}.kitchen:9092"
  end
  zk = '--zookeeper zookeeper-kafka.kitchen/kafka-kitchen'
  bootstrap = '--bootstrap-server '\
    'kafka-kitchen-01.kitchen:9092,'\
    'kafka-kitchen-02.kitchen:9092'
  topic = '--topic test'
  messages = %w[test_msg] * 6
  copts = '--from-beginning --max-messages 6'
  emute = '2> /dev/null'
  fmute = '> /dev/null  2>&1'

  it 'We be able to send message to topic "test"' do
    res = true
    i = 0
    messages.each do |m|
      res &= system(
        "echo #{m} | kafka-console-producer #{brokers[i]} #{topic} #{fmute}"
      )
      i = (i + 1) % 2
    end
    expect(res).to eq(true)
  end

  it 'Topic "test" should have been created' do
    expected = ['Topic:', 'test',
                'PartitionCount:', '2',
                'ReplicationFactor:', '2']
    topics = `kafka-topics --describe #{zk} #{topic} #{emute}`.split[0..5]
    expect(topics).to eq(expected)
  end

  it 'Topic "test" should contain the messages we sent' do
    output = `kafka-console-consumer #{bootstrap} #{topic} #{copts} #{emute}`
    output = output.split.sort
    expect(output).to eq(messages.sort)
  end
end

# rubocop:enable Metrics/BlockLength
